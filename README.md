---
editor_options: 
  markdown: 
    wrap: 72
---

updates: 2022-10-25

## data download

-   enrich_future_simulation_week

# Codes

## global_demand.R

-   First Code to Run ilk calisacak kodlardan. forecast error a gore
    random demand generate ediyor order_range_count parametresini
    degistirerek random sayi artabilir in_use_cycle_scenario parametresi
    de kritik. hangi inuse cycle a gore run edecegimizi soyluyor
-   runtime: 40 minutes in single session. we need to create two global
    demand. so serial run means 40\*2=80 minutes

## existing_orders.R

-   amac musterinin open POlarini kaydetmek.

## buy_decision_order_range_create.R

-   Second Code to Run create order range based on global data.

    -   input:

    -   output: "algorithm/order_range"

-   50 minutes run time

## buy_decision_scenario_create.R

-   mean q30 etc gibi senaryolar olustuyor, dc rebalancei yapiyor

    -   input: algorithm/order_range

    -   output: algorithm/order_scenarios/"global_demand_name"

    -   (*global_demand_name= "Scenario=inuse_distbyHUB_orderengine"*)

## buy_decision_withsim_existing_POs.R

-   dc rebalance on existing pos

## performance_measure.R

-   Third Code to Run Simulate each order scenario and export
    performance

## performance_reporting_dataprep.R

-   4th Code to Run performance_measure'da cikan sonuclari birlestiriyor

## performance_reporting_tableau.R

-   data to tableau

## fc_att_lookup_filemanager.R

-   creates excel files for customer review

## order_lookup_filemanager.R

-   creates oder lookup

## report refresh

-   <https://tailoredbrands.next.inventanalytics.com/reports/FutureVisibilityDashboard/BuyStrategyImpactDashboard>
-   <https://tailoredbrands.next.inventanalytics.com/reports/OrderReviewSheet/OrderReviewSheet>

## report/rental_inuse_hist.R

-   surekli run a gerek yok fakat hub bazli dagitimi cikartiyor
    guncellemek istersek

-   #report refresh
    <https://tailoredbrands.next.inventanalytics.com/reports/InventoryInUseTime/RentalInUseTime>
