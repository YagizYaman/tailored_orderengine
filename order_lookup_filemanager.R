rm(list = ls());gc();
library(data.table)
library(RcppRoll)
library(inventRlibrary)
library(ggplot2)
library(configr)
library(zoo)
library(prophet)
library(writexl)

if (Sys.info()["user"] == "onur.uzun") {
  setwd("C:/users/onur.uzun/tailoredbrands")
  source("initialize.R")
}

if(Sys.info()["user"]=="koray"){
  setwd("C:\\git\\tailoredbrands")
  source("initialize.R")
}

if (Sys.info()["user"] == "yagiz.yaman") {
  setwd("C:/Users/yagiz.yaman/tailoredbrands")
  source("initialize.R")
}

if(Sys.info()["user"]=="simay.colak"){
  setwd("C:\\git\\tailoredbrands")
  source("initialize.R")
}

if(F){
data_access("forecast_lookup")
data_access("attrition_lookup")
if (nrow(forecast_lookup[is.na(ITEM_TYPE_FULL)]) > 0) {
  print("errorrrrr!!!!!!!!!")
}

}
data_access("sim_output_kpis")
order_scenarios=sim_output_kpis
rm(sim_output_kpis)



order_scenarios[,COMPANY:=as.character(COMPANY)]
order_scenarios[,HUB:=as.character(HUB)]
order_scenarios[,STYLE_NO:=as.character(STYLE_NO)]
order_scenarios[,SIZE_CODE:=as.character(SIZE_CODE)]


#forecast_lookup=forecast_lookup[HUB!="ALL"]
if(FALSE){
lookup_future <- forecast_lookup[FISCAL_YEAR==2023]


pair_ordert = order_scenarios[,list(order_table=1),
                              list(COMPANY,STYLE_NO,HUB,SIZE_CODE)]
pair_orderlu = lookup_future[,list(lookuptable=1),
                             list(COMPANY,STYLE_NO,HUB,SIZE_CODE)]
pair_all = rbind(pair_orderlu[,list(COMPANY,
                                    STYLE_NO,
                                    HUB,
                                    SIZE_CODE)],
                 pair_ordert[,list(COMPANY,
                                   STYLE_NO,
                                   HUB,
                                   SIZE_CODE)])

pair_all=pair_all[!duplicated(pair_all)]
setkey(pair_all,COMPANY,STYLE_NO,HUB,SIZE_CODE)
setkey(pair_orderlu,COMPANY,STYLE_NO,HUB,SIZE_CODE)
setkey(pair_ordert,COMPANY,STYLE_NO,HUB,SIZE_CODE)
pair_all=pair_ordert[pair_orderlu[pair_all]]

pair_all[is.na(lookuptable)]


lookup_future=forecast_lookup[FISCAL_YEAR==2023
                              ,list(COMPANY ,
                                    ITEM_TYPE_FULL,
                                    HUB,
                                    STYLE_NO, 
                                    SIZE_CODE,
                                    FCST_4W_Spring = round(FORECAST_4W_Peak_Spring,0),
                                    FCST_5W_Spring = round(FORECAST_5W_Peak_Spring,0),
                                    FCST_4W_Fall = round(FORECAST_4W_Peak_Fall,0),
                                    FCST_5W_Fall = round(FORECAST_5W_Peak_Fall,0)
                              )]

lookup_22 <- forecast_lookup[FISCAL_YEAR==2022]
lookup_22 <- lookup_22[
  ,list(COMPANY ,
        HUB,
        STYLE_NO, 
        SIZE_CODE,
        TD_4W_Spring22 = round(TRUE_DEMAND_4W_Peak_Spring,0),
        TD_4W_Spring22_corrected = round(TRUE_DEMAND_4W_Peak_Spring_corrected,0),
        TD_4W_Fall22 = round(TRUE_DEMAND_4W_Peak_Fall,0) ,
        TD_5W_Spring22 = round(TRUE_DEMAND_5W_Peak_Spring,0),
        TD_5W_Fall22 = round(TRUE_DEMAND_5W_Peak_Fall,0) 
  )]

lookup_19 <- forecast_lookup[FISCAL_YEAR==2019]
lookup_19 <- lookup_19[,list(COMPANY ,
                             HUB,
                             STYLE_NO, 
                             SIZE_CODE,
                             TD_4W_Spring19 = round(TRUE_DEMAND_4W_Peak_Spring,0), 
                             TD_4W_Fall19 = round(TRUE_DEMAND_4W_Peak_Fall,0) ,
                             TD_5W_Spring19 = round(TRUE_DEMAND_5W_Peak_Spring,0),
                             TD_5W_Fall19 = round(TRUE_DEMAND_5W_Peak_Fall,0) 
)]

lookup_21 <- forecast_lookup[FISCAL_YEAR==2021]
lookup_21 <- lookup_21[,list(COMPANY ,
                             HUB,
                             STYLE_NO, 
                             SIZE_CODE,
                             TD_4W_Spring21 = round(TRUE_DEMAND_4W_Peak_Spring,0), 
                             TD_4W_Fall21 = round(TRUE_DEMAND_4W_Peak_Fall,0) ,
                             TD_5W_Spring21 = round(TRUE_DEMAND_5W_Peak_Spring,0),
                             TD_5W_Fall21 = round(TRUE_DEMAND_5W_Peak_Fall,0) 
)]





setkey(lookup_future,COMPANY,HUB,STYLE_NO, SIZE_CODE)
setkey(lookup_22,COMPANY,HUB,STYLE_NO, SIZE_CODE)
setkey(lookup_21,COMPANY,HUB,STYLE_NO, SIZE_CODE)
setkey(lookup_19,COMPANY,HUB,STYLE_NO, SIZE_CODE)

lookup_future=lookup_19[lookup_21[lookup_22[lookup_future]]]


attrition_lookup[Attrition_Period=="Today To Spring Attrition",
                 Attrition_Period:="UntilSpring"
]
attrition_lookup[,Attrition_Forecast:=round(Attrition_Forecast,0)]
attrition_lookup[Attrition_Period=="Spring To Fall Attrition",
                 Attrition_Period:="Spring_to_Fall"
]

att_full_lookup = reshape(attrition_lookup[,list(COMPANY ,
                                                 STYLE_NO ,
                                                 SIZE_CODE ,
                                                 HUB,
                                                 Attrition_Forecast,
                                                 LY_ATTRITION,
                                                 Attrition_Period
)]
, direction = "wide"
, idvar = c("COMPANY",
            "STYLE_NO",
            "SIZE_CODE",
            "HUB")
, timevar = "Attrition_Period")

setkey(att_full_lookup,COMPANY,STYLE_NO,SIZE_CODE,HUB)
setkey(lookup_future,COMPANY,STYLE_NO,SIZE_CODE,HUB)

lookup_future=att_full_lookup[lookup_future]

order_scenarios[,.N,list(run_name_child)]
setnames(order_scenarios,"min_ENDSTOCK","est_excess")


order_scenarios[,.N,list(run_name_child)]

rentable_qty = order_scenarios[period_tag=="no_impact_period"
                               & run_name_child=="Existing_POs"
][,list(HUB,
        STYLE_NO,
        SIZE_CODE,
        Rentable_Start)]
}
order_scenarios[,.N,list(run_name_child)]

order_output_table=copy(order_scenarios)
data_access("pmaster")
pmaster[, ITEM_TYPE_FULL := paste0(ITEM_TYPE, ":", ITEM_TYPE_DESC)]

hierarchytable = pmaster[,list(ITEM_TYPE_FULL=ITEM_TYPE_FULL[1]),
                               list(COMPANY,STYLE_NO)]
hierarchytable2 = pmaster[,list(ITEM_TYPE_FULL2=ITEM_TYPE_FULL[1]),
                         list(STYLE_NO)]

setkey(hierarchytable,COMPANY,STYLE_NO)
setkey(order_output_table,COMPANY,STYLE_NO)
order_output_table=hierarchytable[order_output_table]

setkey(hierarchytable2,STYLE_NO)
setkey(order_output_table,STYLE_NO)
order_output_table=hierarchytable2[order_output_table]

order_output_table[is.na(ITEM_TYPE_FULL),
                   ITEM_TYPE_FULL:=ITEM_TYPE_FULL2 ]
order_output_table[,ITEM_TYPE_FULL2:=NULL]

order_output_table[is.na(ITEM_TYPE_FULL)]


data_access("style_cost_lookup")

setkey(order_output_table,COMPANY,STYLE_NO,SIZE_CODE)
setkey(style_cost_lookup,COMPANY,STYLE_NO,SIZE_CODE)
order_output_table=style_cost_lookup[,list(COMPANY,
                                           STYLE_NO,
                                           SIZE_CODE,
                                           VENDOR_COST)][order_output_table]

style_cost_lookup_2 = style_cost_lookup[,list(VENDOR_COST2=round(mean(VENDOR_COST),2)
),list(COMPANY,STYLE_NO)]

setkey(order_output_table,COMPANY,STYLE_NO)
setkey(style_cost_lookup_2,COMPANY,STYLE_NO)
order_output_table=style_cost_lookup_2[,list(COMPANY,
                                             STYLE_NO,
                                             VENDOR_COST2)][order_output_table]

order_output_table[is.na(VENDOR_COST),VENDOR_COST:=VENDOR_COST2]
order_output_table[,VENDOR_COST2:=NULL]

order_output_table[,it_avgcost:=round(mean(VENDOR_COST,na.rm=T),
                                      2),list(COMPANY,
                                              ITEM_TYPE_FULL)]

order_output_table[is.na(VENDOR_COST),VENDOR_COST:=it_avgcost]
order_output_table[,it_avgcost:=NULL]

order_output_table[,.N,list(report_name)]



order_output_table[HUB==1099 & STYLE_NO==5711
                   & SIZE_CODE=="19/ 35"
                   & run_name_child=="existingpowithRebalance"
                   
                   ]



realfigures_noimpact = order_output_table[run_name_child=="existingpowithRebalance"
                                 & period_tag=="no_impact_period"
                                 ,
                                 list(ITEM_TYPE_FULL,
                                      COMPANY
                                      ,HUB
                                      ,STYLE_NO   
                                      ,SIZE_CODE
                                      ,Rentable_Start
                                      ,Existing_PO_noimpact=PO_Buy
                                      ,TRN_IN
                                      ,TRN_OUT
                                      ,demand_noimpact=all_demand
                                      ,missed_noimpact=missed_demand_allyear
                                 )]

realfigures_fh = order_output_table[run_name_child=="existingpowithRebalance"
                                    & period_tag=="first_half"
                                    ,
                                    list(ITEM_TYPE_FULL,
                                         COMPANY
                                         ,HUB
                                         ,STYLE_NO   
                                         ,SIZE_CODE
                                         ,Existing_PO_FH = PO_Buy
                                         ,demand_FH=all_demand
                                         ,Existing_PO_missed_FH=missed_demand_allyear
                                    )]

realfigures_sh = order_output_table[run_name_child=="existingpowithRebalance"
                                    & period_tag=="second_half"
                                    ,
                                    list(ITEM_TYPE_FULL,
                                         COMPANY
                                         ,HUB
                                         ,STYLE_NO   
                                         ,SIZE_CODE
                                         ,Existing_PO_SH = PO_Buy
                                         ,demand_SH=all_demand
                                         ,Existing_PO_missed_SH=missed_demand_allyear
                                    )]

setkey(realfigures_noimpact,ITEM_TYPE_FULL,COMPANY,HUB,STYLE_NO,SIZE_CODE)
setkey(realfigures_fh,ITEM_TYPE_FULL,COMPANY,HUB,STYLE_NO,SIZE_CODE)
setkey(realfigures_sh,ITEM_TYPE_FULL,COMPANY,HUB,STYLE_NO,SIZE_CODE)

output_figures=realfigures_sh[realfigures_fh[realfigures_noimpact]]

order_output_table[,.N,list(report_name)]

scenariolist_to_join = c("Scenario_1",
                         "Scenario_2",
                         "Scenario_3",
                         "Scenario_4",
                         "Scenario_5",
                         "Scenario_6",
                         "Scenario_7",
                         "Scenario_8")
sc = scenariolist_to_join[1]
for(sc in scenariolist_to_join){
  
  jointable = order_output_table[report_name==sc
                                 & period_tag %in% c("first_half",
                                                     "second_half"
                                                     )
                                 ,
                                 list(ITEM_TYPE_FULL,
                                      COMPANY
                                      ,HUB
                                      ,STYLE_NO   
                                      ,SIZE_CODE
                                      ,period_tag
                                      ,PO_Buy
                                      ,missed_demand_allyear
                                      
                                 )]
  
  jointable = reshape(jointable,
                      direction = "wide",
                      idvar = c("ITEM_TYPE_FULL",
                                "COMPANY",
                                "HUB",
                                "STYLE_NO",
                                "SIZE_CODE"),
                      timevar = "period_tag")
  
  
  setkey(jointable,ITEM_TYPE_FULL,COMPANY,HUB,STYLE_NO,SIZE_CODE)
  setkey(output_figures,ITEM_TYPE_FULL,COMPANY,HUB,STYLE_NO,SIZE_CODE)
  output_figures=jointable[output_figures]
  rm(jointable)
  
  output_figures[,PO_Buy.first_half:=pmax(PO_Buy.first_half-Existing_PO_FH,0)]
  output_figures[,PO_Buy.second_half:=pmax(PO_Buy.second_half-Existing_PO_SH,0)]
  
  setnames(output_figures,"PO_Buy.first_half",paste0("PO_FH_",gsub("enario_","",sc)))
  setnames(output_figures,"PO_Buy.second_half",paste0("PO_SH_",gsub("enario_","",sc)))
  setnames(output_figures,"missed_demand_allyear.first_half",paste0("miss_FH_",gsub("enario_","",sc)))
  setnames(output_figures,"missed_demand_allyear.second_half",paste0("miss_SH_",gsub("enario_","",sc)))
  
  
 
  
}



sum(output_figures[COMPANY=="TMW" & STYLE_NO==5711]$Existing_PO_FH)

data_access("product_enrich")
setkey(output_figures, COMPANY, STYLE_NO, SIZE_CODE)
setkey(product_enrich, COMPANY, STYLE_NO, SIZE_CODE)
output_figures <- product_enrich[
  , .(COMPANY, STYLE_NO, SIZE_CODE, SIZE_FAMILY)
][output_figures]
output_figures_s=output_figures[,list(ITEM_TYPE_FULL 
                                    ,COMPANY  
                                    ,HUB 
                                    ,STYLE_NO 
                                    ,SIZE_CODE
                                    ,SIZE_FAMILY
                                    ,Rentable_Start 
                                    ,TRN_IN 
                                    ,TRN_OUT
                                    ,demand_noimpact
                                    ,demand_FH
                                    ,demand_SH
                                    ,Existing_PO_noimpact 
                                    ,Existing_PO_FH
                                    ,Existing_PO_SH
                                    ,PO_FH_Sc8 
                                    ,PO_FH_Sc7
                                    ,PO_FH_Sc6 
                                    ,PO_FH_Sc5 
                                    ,PO_FH_Sc4 
                                    ,PO_FH_Sc3
                                    ,PO_FH_Sc2 
                                    ,PO_FH_Sc1 
                                    
                                    ,PO_SH_Sc8 
                                    ,PO_SH_Sc7 
                                    ,PO_SH_Sc6 
                                    ,PO_SH_Sc5 
                                    ,PO_SH_Sc4 
                                    ,PO_SH_Sc3 
                                    ,PO_SH_Sc2 
                                    ,PO_SH_Sc1 
                                    
                                    ,Existing_PO_missed_FH 
                                    ,miss_FH_Sc8
                                    ,miss_FH_Sc7
                                    ,miss_FH_Sc6
                                    ,miss_FH_Sc5
                                    ,miss_FH_Sc4
                                    ,miss_FH_Sc3
                                    ,miss_FH_Sc2
                                    ,miss_FH_Sc1
                                    
                                    ,Existing_PO_missed_SH
                                    ,miss_SH_Sc8
                                    ,miss_SH_Sc7
                                    ,miss_SH_Sc6
                                    ,miss_SH_Sc5
                                    ,miss_SH_Sc4
                                    ,miss_SH_Sc3
                                    ,miss_SH_Sc2
                                    ,miss_SH_Sc1
                                    )]



output_figures_s[,STYLE_NO2:=STYLE_NO]
sum(output_figures[COMPANY=="TMW" & STYLE_NO==5711]$Existing_PO_FH)

fwrite(output_figures_s,file=file.path(tableau_path,"order_scenarios_wide.csv"))




setkey(order_output_table, COMPANY, STYLE_NO, SIZE_CODE)
setkey(product_enrich, COMPANY, STYLE_NO, SIZE_CODE)
order_output_table <- product_enrich[
  , .(COMPANY, STYLE_NO, SIZE_CODE, SIZE_FAMILY)
][order_output_table]


order_output_table_s = order_output_table[period_tag != "no_impact_period",
                                          list(ITEM_TYPE_FULL,
                                               COMPANY,
                                               HUB,
                                               STYLE_NO,
                                               SIZE_CODE,
                                               SIZE_FAMILY,
                                               report_name,
                                               period_tag,
                                               all_demand_AprilMay,
                                               missed_demand_AprilMay,
                                               all_demand_October,
                                               missed_demand_October,
                                               all_demand,
                                               missed_demand_allyear,
                                               PO_Buy,
                                               Rentable_Start,
                                               estimated_not_used_inventory = min_ENDSTOCK,
                                               COST = VENDOR_COST
                                          )]



write_to_blob(order_output_table_s,
              savename = "orderlook_forecast.parquet",
              containerconnection = container_datastore,
              blobfilepath = "algorithm/filemanager/excel/latest/",
              format ="parquet"
)

write_to_blob(order_output_table_s,
              savename = "orderlook_forecast.parquet",
              containerconnection = container_datastore,
              blobfilepath = paste0("algorithm/filemanager/excel/",Sys.Date()),
              format ="parquet"
)


order_output_table_s[,savename:=paste0(COMPANY,"_",ITEM_TYPE_FULL)]

looplist = unique(order_output_table_s$savename)
ll = looplist[1]
for(ll in looplist){
  
  aa=order_output_table_s[savename==ll]
  aa[,savename:=NULL]
  ll=gsub(":","_",ll)
  fwrite(aa,file=paste0(excelpath,"/",ll,"_order.csv"))
  
  
}



setnames(order_output_table_s, "ITEM_TYPE_FULL", "ITEM_TYPE")

estimated_not_use_detail <- order_output_table_s[
  period_tag == "first_half",
  .(PO_Buy = sum(PO_Buy, na.rm = T)),
  .(COMPANY, ITEM_TYPE, STYLE_NO, SIZE_CODE, SIZE_FAMILY, report_name)]

estimated_not_use_detail_casted <- dcast.data.table(
  data = estimated_not_use_detail,
  formula = COMPANY + ITEM_TYPE + STYLE_NO + SIZE_FAMILY + SIZE_CODE ~ report_name,
  value.var = c("PO_Buy"),
)
org_cols <- colnames(estimated_not_use_detail_casted)
org_cols <- org_cols[org_cols %like% "Scenario" | org_cols %like% "Existing"]
setnames(estimated_not_use_detail_casted, 
         org_cols, 
         c(paste0("PO_Buy_Style_Size_", org_cols)))
estimated_not_use_detail_casted[is.na(estimated_not_use_detail_casted)] <- 0

estimated_not_use <- order_output_table_s[
  period_tag == "first_half",
  .(PO_Buy = sum(PO_Buy, na.rm = T),
    estimated_not_used_inventory = sum(estimated_not_used_inventory,
                                       na.rm = T)),
  .(COMPANY, ITEM_TYPE, STYLE_NO, SIZE_FAMILY, report_name)]

estimated_not_use_casted <- dcast.data.table(
  data = estimated_not_use,
  formula = COMPANY + ITEM_TYPE + STYLE_NO + SIZE_FAMILY ~ report_name,
  value.var = c("PO_Buy", "estimated_not_used_inventory"),
  
)

estimated_not_use_casted[is.na(estimated_not_use_casted)] <- 0

for (i in c(paste0("Scenario_", 1:8))) {
  col_name <- paste0("incremental_", i)
  po_col <- paste0("PO_Buy_", i)
  alert_col <- paste0("family_need_", i)
  alert_bucket_col <- paste0("family_need_bucket_", i)
  estimated_not_use_casted[, c(col_name) := get(po_col) - `PO_Buy_Existing POs`]
  estimated_not_use_casted[
    , c(alert_col) := get(col_name) -
      `estimated_not_used_inventory_Existing POs with DC Rebalance`]
  estimated_not_use_casted[, c(alert_bucket_col) := cut(
    get(alert_col),
    c(-Inf, 0, 50, 100, 500, 1000, Inf))]
}


setkey(estimated_not_use_detail_casted, COMPANY, ITEM_TYPE, STYLE_NO, SIZE_FAMILY)
setkey(estimated_not_use_casted, COMPANY, ITEM_TYPE, STYLE_NO, SIZE_FAMILY)
estimated_not_use_detail_casted <- estimated_not_use_casted[estimated_not_use_detail_casted]

fwrite(estimated_not_use_detail_casted,
       file = file.path(tableau_path, "size_family_additional_buy_tableau.csv"))







