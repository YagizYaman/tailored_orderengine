rm(list=ls())
library(data.table)
library(RcppRoll)
library(configr)
library(AzureStor)
library(inventRlibrary)


if (Sys.info()["user"] == "onur.uzun") {
  setwd("C:/users/onur.uzun/tailoredbrands")
  source("initialize.R")
}

if(Sys.info()["user"]=="koray"){
  setwd("C:\\git\\tailoredbrands")
  source("initialize.R")
}

if (Sys.info()["user"] == "yagiz.yaman") {
  setwd("C:/Users/yagiz.yaman/tailoredbrands")
  source("initialize.R")
}

if(Sys.info()["user"]=="simay.colak"){
  setwd("C:\\git\\tailoredbrands")
  source("initialize.R")
}


global_demand_name = "Scenario=inuse_distbyHUB_orderengine"


order_range_list = AzureStor::list_blobs(container_datastore,
                                 dir=file.path("algorithm/order_range",
                                               global_demand_name),
                                 prefix=NULL,
                                 recursive = T)$name



if(FALSE){
  
  style_list <- c(
    "1130", "1150", "1219", "1317", "1859", "1869", "1892", "1895", 
    "1901", "1912", "1919", "2160", "2171", "2219", "2850", "2901", 
    "2912", "2917", "4500", "4700")
  
  string_expr <- ""
  for (i in style_list) {
    if (string_expr == "") {
      string_expr <- paste0(string_expr, "order_range_list %like% '", i, "'")
    } else {
      string_expr <- paste0(string_expr, " | order_range_list %like% '", i, "'")
    }
  }
  
  string_expr <- paste0("order_range_list[", string_expr, "]")
  order_range_list <- eval(parse(text = string_expr))
  
  order_range_list=order_range_list[order_range_list%like% '2130']
  
  order_range_list = order_range_list[order_range_list %like% "2130" |
                                        order_range_list %like% "2219" |
                                        order_range_list %like% "2850" |
                                        order_range_list %like% "2912" |
                                        order_range_list %like% "1130" |
                                        order_range_list %like% "1180" |
                                        order_range_list %like% "1219" |
                                        order_range_list %like% "1859" |
                                        order_range_list %like% "1869" |
                                        order_range_list %like% "1980"
                            
  ]
}
order_range=order_range_list[1]

for(order_range in order_range_list){
  
  simoutput_master = read_parquetR(pathtoread = file.path(order_range),
                                   containerconnection = container_datastore,
                                   filelocation = "azure"
  )
  
  
  simoutput_master[,periodtag:=ifelse(WEEK_START_DATE<first_half_datelimit,
                                      "first_half","second_half"
  )]
  
  simoutput_master = simoutput_master[WEEK_START_DATE<='2023-12-31']
  
  data_access("max_peak_fc_dynamic")
  
  setkey(simoutput_master, COMPANY, HUB, STYLE_NO, SIZE_CODE, periodtag)
  setkey(max_peak_fc_dynamic, COMPANY, HUB, STYLE_NO, SIZE_CODE, periodtag)
  simoutput_master <- max_peak_fc_dynamic[
    , .(COMPANY, HUB, STYLE_NO, SIZE_CODE, periodtag,
        PeakDemand_rolling_TRN = round(PeakForecast_rolling))
  ][simoutput_master]
  
  
  buy_scenario_mean = find_buy_scenario(input_data = simoutput_master,
                                        calc_type = "mean")
  
  
  buy_scenario_mean <- excel_like_rebalance(buy_scenario =  buy_scenario_mean)
  
  buy_scenario_mean[,periodtag:=ifelse(WEEK_START_DATE<first_half_datelimit,
                                       "first_half","second_half"
  )]
  
  buy_scenario_mean_rb = rebalance_them_all(
    inputdata = buy_scenario_mean[,list(COMPANY,
                                        HUB,
                                        STYLE_NO,
                                        SIZE_CODE,
                                        WEEK_START_DATE,
                                        periodtag,
                                        PeakDemand_rolling,
                                        PeakDemand_rolling_TRN,
                                        ATTRITION,
                                        PO_RECEIVED=PO_RECEIVED+BUY,
                                        RENTABLE,
                                        TRN_IN,
                                        TRN_OUT
    )])
  setkey(buy_scenario_mean,COMPANY,HUB,STYLE_NO,SIZE_CODE,WEEK_START_DATE)
  setkey(buy_scenario_mean_rb,COMPANY,HUB,STYLE_NO,SIZE_CODE,WEEK_START_DATE)
  
  buy_scenario_mean[,c("TRN_IN","TRN_OUT"):=NULL]
  
  buy_scenario_mean=buy_scenario_mean_rb[,list(COMPANY,HUB,
                                               STYLE_NO,
                                               SIZE_CODE,
                                               WEEK_START_DATE,
                                               TRN_IN,
                                               TRN_OUT
  )][buy_scenario_mean]
  rm(buy_scenario_mean_rb)
  
  buy_scenario_q30 = find_buy_scenario(input_data = simoutput_master,
                                       calc_type = "quant",
                                       param = 30)
  buy_scenario_q30 <- excel_like_rebalance(buy_scenario =  buy_scenario_q30)
  buy_scenario_q30[,periodtag:=ifelse(WEEK_START_DATE<first_half_datelimit,
                                      "first_half","second_half"
  )]
  
  buy_scenario_q30_rb = rebalance_them_all(
    inputdata=buy_scenario_q30[,list(COMPANY,
                                     HUB,
                                     STYLE_NO,
                                     SIZE_CODE,
                                     WEEK_START_DATE,
                                     periodtag,
                                     PeakDemand_rolling,
                                     PeakDemand_rolling_TRN,
                                     ATTRITION,
                                     PO_RECEIVED=PO_RECEIVED+BUY,
                                     RENTABLE,
                                     TRN_IN,
                                     TRN_OUT
  )])
  buy_scenario_q30[,c("TRN_IN","TRN_OUT"):=NULL]
  
  setkey(buy_scenario_q30,COMPANY,HUB,STYLE_NO,SIZE_CODE,WEEK_START_DATE)
  setkey(buy_scenario_q30_rb,COMPANY,HUB,STYLE_NO,SIZE_CODE,WEEK_START_DATE)
  buy_scenario_q30=buy_scenario_q30_rb[,list(COMPANY,HUB,
                                             STYLE_NO,
                                             SIZE_CODE,
                                             WEEK_START_DATE,
                                             TRN_IN,
                                             TRN_OUT
  )][buy_scenario_q30]
  rm(buy_scenario_q30_rb)
  
  
  
  
  
  ### q80
  
  
  buy_scenario_q80 = find_buy_scenario(input_data = simoutput_master,
                                       calc_type = "quant",
                                       param = 80)
  buy_scenario_q80 <- excel_like_rebalance(buy_scenario =  buy_scenario_q80)
  
  
  buy_scenario_q80[,periodtag:=ifelse(WEEK_START_DATE<first_half_datelimit,
                                      "first_half","second_half"
  )]

  buy_scenario_q80_rb = rebalance_them_all(
    inputdata=buy_scenario_q80[,list(COMPANY,
                                     HUB,
                                     STYLE_NO,
                                     SIZE_CODE,
                                     WEEK_START_DATE,
                                     periodtag,
                                     PeakDemand_rolling,
                                     PeakDemand_rolling_TRN,
                                     ATTRITION,
                                     PO_RECEIVED=PO_RECEIVED+BUY,
                                     RENTABLE,
                                     TRN_IN,
                                     TRN_OUT
  )])
  
  buy_scenario_q80[,c("TRN_IN","TRN_OUT"):=NULL]
  
  setkey(buy_scenario_q80,COMPANY,HUB,STYLE_NO,SIZE_CODE,WEEK_START_DATE)
  setkey(buy_scenario_q80_rb,COMPANY,HUB,STYLE_NO,SIZE_CODE,WEEK_START_DATE)
  buy_scenario_q80=buy_scenario_q80_rb[,list(COMPANY,HUB,
                                             STYLE_NO,
                                             SIZE_CODE,
                                             WEEK_START_DATE,
                                             TRN_IN,
                                             TRN_OUT
  )][buy_scenario_q80]
  rm(buy_scenario_q80_rb)
  
  
  
  
  
  
  # q95
  
  buy_scenario_q95 = find_buy_scenario(input_data = simoutput_master,
                                       calc_type = "quant",
                                       param = 95)
  buy_scenario_q95 <- excel_like_rebalance(buy_scenario =  buy_scenario_q95)
  
  
  buy_scenario_q95[,periodtag:=ifelse(WEEK_START_DATE<first_half_datelimit,
                                      "first_half","second_half"
  )]
  # buy_scenario_q95[,c("TRN_IN","TRN_OUT"):=NULL]
  buy_scenario_q95_rb = rebalance_them_all(
    inputdata=buy_scenario_q95[,list(COMPANY,
                                     HUB,
                                     STYLE_NO,
                                     SIZE_CODE,
                                     WEEK_START_DATE,
                                     periodtag,
                                     PeakDemand_rolling,
                                     PeakDemand_rolling_TRN,
                                     ATTRITION,
                                     PO_RECEIVED=PO_RECEIVED+BUY,
                                     RENTABLE,
                                     TRN_IN,
                                     TRN_OUT
    )])
  buy_scenario_q95[,c("TRN_IN","TRN_OUT"):=NULL]
  
  
  setkey(buy_scenario_q95,COMPANY,HUB,STYLE_NO,SIZE_CODE,WEEK_START_DATE)
  setkey(buy_scenario_q95_rb,COMPANY,HUB,STYLE_NO,SIZE_CODE,WEEK_START_DATE)
  buy_scenario_q95=buy_scenario_q95_rb[,list(COMPANY,HUB,
                                             STYLE_NO,
                                             SIZE_CODE,
                                             WEEK_START_DATE,
                                             TRN_IN,
                                             TRN_OUT
  )][buy_scenario_q95]
  rm(buy_scenario_q95_rb)
  
  
  # less10
  
  buy_scenario_mean_less10 = copy(buy_scenario_mean)
  buy_scenario_mean_less10[,BUY:=round(BUY*0.90)]
  
  buy_scenario_mean_less10[,periodtag:=ifelse(WEEK_START_DATE<first_half_datelimit,
                                              "first_half","second_half"
  )]
  # buy_scenario_mean_less10[,c("TRN_IN","TRN_OUT"):=NULL]
  buy_scenario_mean_less10_rb = rebalance_them_all(
    inputdata=buy_scenario_mean_less10[,list(COMPANY,
                                             HUB,
                                             STYLE_NO,
                                             SIZE_CODE,
                                             WEEK_START_DATE,
                                             periodtag,
                                             PeakDemand_rolling,
                                             PeakDemand_rolling_TRN,
                                             ATTRITION,
                                             PO_RECEIVED=PO_RECEIVED+BUY,
                                             RENTABLE,
                                             TRN_IN,
                                             TRN_OUT
    )])
  
  buy_scenario_mean_less10[,c("TRN_IN","TRN_OUT"):=NULL]
  
  setkey(buy_scenario_mean_less10,COMPANY,HUB, STYLE_NO,
         SIZE_CODE, WEEK_START_DATE)
  setkey(buy_scenario_mean_less10_rb,COMPANY,HUB, STYLE_NO,
         SIZE_CODE, WEEK_START_DATE)
  buy_scenario_mean_less10=buy_scenario_mean_less10_rb[,list(COMPANY,HUB,
                                                             STYLE_NO,
                                                             SIZE_CODE,
                                                             WEEK_START_DATE,
                                                             TRN_IN,
                                                             TRN_OUT
  )][buy_scenario_mean_less10]
  rm(buy_scenario_mean_less10_rb)
  
  
  
  
  # less 20  
  
  
  buy_scenario_mean_less20 = copy(buy_scenario_mean)
  buy_scenario_mean_less20[,BUY:=round(BUY*0.80)]
  
  buy_scenario_mean_less20[,periodtag:=ifelse(WEEK_START_DATE<first_half_datelimit,
                                              "first_half","second_half"
  )]
  # buy_scenario_mean_less20[,c("TRN_IN","TRN_OUT"):=NULL]
  buy_scenario_mean_less20_rb = rebalance_them_all(
    inputdata=buy_scenario_mean_less20[,list(COMPANY,
                                             HUB,
                                             STYLE_NO,
                                             SIZE_CODE,
                                             WEEK_START_DATE,
                                             periodtag,
                                             PeakDemand_rolling,
                                             PeakDemand_rolling_TRN,
                                             ATTRITION,
                                             PO_RECEIVED=PO_RECEIVED+BUY,
                                             RENTABLE,
                                             TRN_IN,
                                             TRN_OUT
    )])
  
  buy_scenario_mean_less20[,c("TRN_IN","TRN_OUT"):=NULL]
  
  setkey(buy_scenario_mean_less20,COMPANY,HUB,STYLE_NO,
         SIZE_CODE, WEEK_START_DATE)
  setkey(buy_scenario_mean_less20_rb,COMPANY,HUB,STYLE_NO,
         SIZE_CODE, WEEK_START_DATE)
  buy_scenario_mean_less20 = buy_scenario_mean_less20_rb[,list(COMPANY,HUB,
                                                               STYLE_NO,
                                                               SIZE_CODE,
                                                               WEEK_START_DATE,
                                                               TRN_IN,
                                                               TRN_OUT
  )][buy_scenario_mean_less20]
  rm(buy_scenario_mean_less20_rb)
  
  
  # less 30 
  buy_scenario_mean_less30 = copy(buy_scenario_mean)
  buy_scenario_mean_less30[,BUY:=round(BUY*0.70,0)]
  
  buy_scenario_mean_less30[,periodtag:=ifelse(WEEK_START_DATE<first_half_datelimit,
                                              "first_half","second_half"
  )]
  # buy_scenario_mean_less30[,c("TRN_IN","TRN_OUT"):=NULL]
  buy_scenario_mean_less30_rb = rebalance_them_all(
    inputdata=buy_scenario_mean_less30[,list(COMPANY,
                                             HUB,
                                             STYLE_NO,
                                             SIZE_CODE,
                                             WEEK_START_DATE,
                                             periodtag,
                                             PeakDemand_rolling,
                                             PeakDemand_rolling_TRN,
                                             ATTRITION,
                                             PO_RECEIVED=PO_RECEIVED+BUY,
                                             RENTABLE,
                                             TRN_IN,
                                             TRN_OUT
    )])
  
  buy_scenario_mean_less30[,c("TRN_IN","TRN_OUT"):=NULL]
  
  setkey(buy_scenario_mean_less30,COMPANY,HUB,STYLE_NO,SIZE_CODE,WEEK_START_DATE)
  setkey(buy_scenario_mean_less30_rb,COMPANY,HUB,STYLE_NO,SIZE_CODE,WEEK_START_DATE)
  buy_scenario_mean_less30=buy_scenario_mean_less30_rb[,list(COMPANY,HUB,
                                                             STYLE_NO,
                                                             SIZE_CODE,
                                                             WEEK_START_DATE,
                                                             TRN_IN,
                                                             TRN_OUT
  )][buy_scenario_mean_less30]
  rm(buy_scenario_mean_less30_rb)
  
  
  
  
  # less 50 
  
  buy_scenario_mean_less50 = copy(buy_scenario_mean)
  buy_scenario_mean_less50[,BUY:=round(BUY*0.50,0)]
  
  buy_scenario_mean_less50[,periodtag:=ifelse(WEEK_START_DATE<first_half_datelimit,
                                              "first_half","second_half"
  )]
  # buy_scenario_mean_less50[,c("TRN_IN","TRN_OUT"):=NULL]
  buy_scenario_mean_less50_rb = rebalance_them_all(
    inputdata=buy_scenario_mean_less50[,list(COMPANY,
                                             HUB,
                                             STYLE_NO,
                                             SIZE_CODE,
                                             WEEK_START_DATE,
                                             periodtag,
                                             PeakDemand_rolling,
                                             PeakDemand_rolling_TRN,
                                             ATTRITION,
                                             PO_RECEIVED=PO_RECEIVED+BUY,
                                             RENTABLE,
                                             TRN_IN,
                                             TRN_OUT
    )])
  
  buy_scenario_mean_less50[,c("TRN_IN","TRN_OUT"):=NULL]
  
  setkey(buy_scenario_mean_less50,COMPANY,HUB,STYLE_NO,SIZE_CODE,WEEK_START_DATE)
  setkey(buy_scenario_mean_less50_rb,COMPANY,HUB,STYLE_NO,SIZE_CODE,WEEK_START_DATE)
  buy_scenario_mean_less50=buy_scenario_mean_less50_rb[,list(COMPANY,HUB,
                                                             STYLE_NO,
                                                             SIZE_CODE,
                                                             WEEK_START_DATE,
                                                             TRN_IN,
                                                             TRN_OUT
  )][buy_scenario_mean_less50]
  rm(buy_scenario_mean_less50_rb)
  
  
  
  all_together = rbind(cbind(buy_scenario_mean,type="mean"),
                       cbind(buy_scenario_q30,type="q30"),
                       cbind(buy_scenario_q80,type="q80"),
                       cbind(buy_scenario_q95,type="q95"),
                       cbind(buy_scenario_mean_less10,type="less10"),
                       cbind(buy_scenario_mean_less20,type="less20"),
                       cbind(buy_scenario_mean_less30,type="less30"),
                       cbind(buy_scenario_mean_less50,type="less50"))
  
  all_together_sort=all_together[,list(tot_value=sum(PO_RECEIVED+BUY)
  ),list(COMPANY,
         HUB,
         STYLE_NO,
         SIZE_CODE,
         type)]
  
  
  all_together_sort=all_together_sort[order(COMPANY,
                                            HUB,
                                            STYLE_NO,
                                            SIZE_CODE,
                                            tot_value)]
  all_together_sort[,rank:=1:.N,list(COMPANY,
                                     HUB,
                                     STYLE_NO,
                                     SIZE_CODE)]
  
  all_together_sort[HUB==5013 & STYLE_NO==1130 & SIZE_CODE=="42S SLIMFIT"]
  
  
  all_together_sort[,type2:="na"]
  all_together_sort[rank==1,type2:="less50"]
  all_together_sort[rank==2,type2:="less30"]
  all_together_sort[rank==3,type2:="less20"]
  all_together_sort[rank==4,type2:="q30"]
  all_together_sort[rank==5,type2:="less10"]
  all_together_sort[rank==6,type2:="mean"]
  all_together_sort[rank==7,type2:="q80"]
  all_together_sort[rank==8,type2:="q95"]
  
  setkey(all_together_sort,COMPANY,HUB,STYLE_NO,SIZE_CODE,type)
  setkey(all_together,COMPANY,HUB,STYLE_NO,SIZE_CODE,type)
  
  all_together=all_together_sort[,list(COMPANY,
                                       HUB,
                                       STYLE_NO,
                                       SIZE_CODE,
                                       type,
                                       type2)][all_together]
  
  
  all_together[COMPANY=="TMW" & SIZE_CODE=="43R SLIMFIT" & HUB=="1099"]
  all_together[type2=="na"]
  all_together[,type:=type2]
  all_together[,type2:=NULL]
  
  buy_scenario_mean = all_together[type=="mean",list(COMPANY  
                                         ,HUB 
                                         ,STYLE_NO 
                                         ,SIZE_CODE   
                                         ,WEEK_START_DATE 
                                         ,TRN_IN
                                         ,TRN_OUT
                                         ,PO_RECEIVED
                                         ,BUY
                                         ,PeakDemand_rolling
                                         ,RENTABLE 
                                         ,ATTRITION
                                         ,periodtag)]
  
  
  buy_scenario_q30 = all_together[type=="q30",list(COMPANY  
                                                    ,HUB 
                                                    ,STYLE_NO 
                                                    ,SIZE_CODE   
                                                    ,WEEK_START_DATE 
                                                    ,TRN_IN
                                                    ,TRN_OUT
                                                    ,PO_RECEIVED
                                                    ,BUY
                                                    ,PeakDemand_rolling
                                                    ,RENTABLE 
                                                    ,ATTRITION
                                                    ,periodtag)]
  
  buy_scenario_q80 = all_together[type=="q80",list(COMPANY  
                                                    ,HUB 
                                                    ,STYLE_NO 
                                                    ,SIZE_CODE   
                                                    ,WEEK_START_DATE 
                                                    ,TRN_IN
                                                    ,TRN_OUT
                                                    ,PO_RECEIVED
                                                    ,BUY
                                                    ,PeakDemand_rolling
                                                    ,RENTABLE 
                                                    ,ATTRITION
                                                    ,periodtag)]
  
  buy_scenario_q95 = all_together[type=="q95",list(COMPANY  
                                                    ,HUB 
                                                    ,STYLE_NO 
                                                    ,SIZE_CODE   
                                                    ,WEEK_START_DATE 
                                                    ,TRN_IN
                                                    ,TRN_OUT
                                                    ,PO_RECEIVED
                                                    ,BUY
                                                    ,PeakDemand_rolling
                                                    ,RENTABLE 
                                                    ,ATTRITION
                                                    ,periodtag)]
  
  buy_scenario_mean_less10 = all_together[type=="less10",list(COMPANY  
                                                            ,HUB 
                                                            ,STYLE_NO 
                                                            ,SIZE_CODE   
                                                            ,WEEK_START_DATE 
                                                            ,TRN_IN
                                                            ,TRN_OUT
                                                            ,PO_RECEIVED
                                                            ,BUY
                                                            ,PeakDemand_rolling
                                                            ,RENTABLE 
                                                            ,ATTRITION
                                                            ,periodtag)]
  
  buy_scenario_mean_less20 = all_together[type=="less20",list(COMPANY  
                                                            ,HUB 
                                                            ,STYLE_NO 
                                                            ,SIZE_CODE   
                                                            ,WEEK_START_DATE 
                                                            ,TRN_IN
                                                            ,TRN_OUT
                                                            ,PO_RECEIVED
                                                            ,BUY
                                                            ,PeakDemand_rolling
                                                            ,RENTABLE 
                                                            ,ATTRITION
                                                            ,periodtag)]
  
  buy_scenario_mean_less30 = all_together[type=="less30",list(COMPANY  
                                                            ,HUB 
                                                            ,STYLE_NO 
                                                            ,SIZE_CODE   
                                                            ,WEEK_START_DATE 
                                                            ,TRN_IN
                                                            ,TRN_OUT
                                                            ,PO_RECEIVED
                                                            ,BUY
                                                            ,PeakDemand_rolling
                                                            ,RENTABLE 
                                                            ,ATTRITION
                                                            ,periodtag)]
  
  buy_scenario_mean_less50 = all_together[type=="less50",list(COMPANY  
                                                            ,HUB 
                                                            ,STYLE_NO 
                                                            ,SIZE_CODE   
                                                            ,WEEK_START_DATE 
                                                            ,TRN_IN
                                                            ,TRN_OUT
                                                            ,PO_RECEIVED
                                                            ,BUY
                                                            ,PeakDemand_rolling
                                                            ,RENTABLE 
                                                            ,ATTRITION
                                                            ,periodtag)]
  
  
  scenario_save_to_blob(input = buy_scenario_mean, naming = "_mean")
  scenario_save_to_blob(input = buy_scenario_q30, naming = "_q30")
  scenario_save_to_blob(input = buy_scenario_q80, naming = "_q80")
  scenario_save_to_blob(input = buy_scenario_q95, naming = "_q95")
  scenario_save_to_blob(input = buy_scenario_mean_less10, naming = "_less10")
  scenario_save_to_blob(input = buy_scenario_mean_less20, naming = "_less20")
  scenario_save_to_blob(input = buy_scenario_mean_less30, naming = "_less30")
  scenario_save_to_blob(input = buy_scenario_mean_less50, naming = "_less50")
  
  rm(buy_scenario_mean,
     buy_scenario_q30,
     buy_scenario_q80,
     buy_scenario_q95,
     buy_scenario_mean_less10,
     buy_scenario_mean_less20,
     buy_scenario_mean_less30,
     buy_scenario_mean_less50
  );gc();
  
  
  
  
}








print("buy_decision_scenario_create.R is completed")









