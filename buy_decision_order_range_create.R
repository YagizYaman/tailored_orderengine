rm(list=ls())
library(data.table)
library(RcppRoll)
library(configr)
library(AzureStor)
library(inventRlibrary)


if (Sys.info()["user"] == "onur.uzun") {
  setwd("C:/users/onur.uzun/tailoredbrands")
  source("initialize.R")
}

if(Sys.info()["user"]=="koray"){
  setwd("C:\\git\\tailoredbrands")
  source("initialize.R")
}

if (Sys.info()["user"] == "yagiz.yaman") {
  setwd("C:/Users/yagiz.yaman/tailoredbrands")
  source("initialize.R")
}

if(Sys.info()["user"]=="simay.colak"){
  setwd("C:\\git\\tailoredbrands")
  source("initialize.R")
}

source(file.path(gitpath,"order_engine/simulateWith_C_v7SeparatingV1.R"))

### CORE PARAMETERS - START

global_demand_name = "Scenario=inuse_distbyHUB_orderengine"

order_range_count = 50
### CORE PARAMETERS - END

#in_use_cycle_groups = list(3,4,5,6,7)

data_access("future_inventory")
buy_decision_data_master=future_inventory[,list(COMPANY
                                                ,HUB
                                                ,STYLE_NO
                                                ,SIZE_CODE
                                                ,WEEK_START_DATE
                                                ,most_recent_rentable 
                                                ,PO_RECEIVE
                                                ,is_future_date
                                                )]


# sutunlari belirt. isfuturedate eklendi. KORAY
rm(future_inventory)
## burasi pazar hatali calisiyor
simrundate = min(buy_decision_data_master[is_future_date==1]$WEEK_START_DATE)
simrundate
buy_decision_data_master = buy_decision_data_master[WEEK_START_DATE >= simrundate]
buy_decision_data_master[STYLE_NO==2130 & 
                           SIZE_CODE=="34R SLIMFIT" & 
                           HUB==1099][order(WEEK_START_DATE)]


buy_decision_data_master[WEEK_START_DATE > simrundate,
                         most_recent_rentable:=0]


buy_decision_data_master[WEEK_START_DATE>=use_existing_orders_until,
                         PO_RECEIVE:=0]


run_list = AzureStor::list_blobs(container_datastore,
                                 dir=file.path("algorithm/global_demand",
                                               global_demand_name),
                                 prefix=NULL,
                                 recursive = T)$name

run_list=sort(run_list)

if(FALSE){
  # for a specific run
  
  style_list <- c(
    "1130", "1150", "1219", "1317", "1859", "1869", "1892", "1895", 
    "1901", "1912", "1919", "2160", "2171", "2219", "2850", "2901", 
    "2912", "2917", "4500", "4700")
  
  string_expr <- ""
  for (i in style_list) {
    if (string_expr == "") {
      string_expr <- paste0(string_expr, "run_list %like% '", i, "'")
    } else {
      string_expr <- paste0(string_expr, " | run_list %like% '", i, "'")
    }
  }
  
  string_expr <- paste0("run_list[", string_expr, "]")
  run_list <- eval(parse(text = string_expr))
  
  run_list = run_list[run_list %like% '2130']
  
  run_list = run_list[run_list %like% "2130" |
                        run_list %like% "2219" |
                        run_list %like% "2850" |
                        run_list %like% "2912" |
                        run_list %like% "1130" |
                        run_list %like% "1180" |
                        run_list %like% "1219" |
                        run_list %like% "1859" |
                        run_list %like% "1869" |
                        run_list %like% "1980"
                      ]
  
  baseline_correctlist = c("1219",
                           "1391",
                           "1859",
                           "2219",
                           "2391",
                           "2965")
  
  run_list=run_list[1:50]
  
}

runrun=run_list[1]
for(runrun in run_list){
  print(runrun)
  global_demand = read_parquetR(pathtoread=file.path(runrun),
                                containerconnection=container_datastore,
                                filelocation = "azure"
  )
  
  join_scope = global_demand[,.N,list(COMPANY,HUB,STYLE_NO,SIZE_CODE)]
  join_scope[,N:=NULL]
  setkey(join_scope,COMPANY,STYLE_NO,SIZE_CODE,HUB)
  setkey(buy_decision_data_master,COMPANY,STYLE_NO,SIZE_CODE,HUB)
  
  buy_decision_data = join_scope[buy_decision_data_master,
                                 nomatch=0]
  rm(join_scope)
  global_demand = global_demand[runid<=order_range_count]
  
  global_demand[,c("error",
                   "forecast_with_error",
                   "forecast"):=NULL]
  setkey(global_demand,
         COMPANY,
         HUB,
         STYLE_NO,
         SIZE_CODE,
         WEEK_START_DATE)
  setkey(buy_decision_data,
         COMPANY,
         HUB,
         STYLE_NO,
         SIZE_CODE,
         WEEK_START_DATE)
  
  buy_decision_data = buy_decision_data[global_demand]
  rm(global_demand);gcQuiet()
  buy_decision_data[is.na(PO_RECEIVE),PO_RECEIVE:=0]
  buy_decision_data=buy_decision_data[WEEK_START_DATE>=simrundate]
  
  buy_decision_data[is.na(most_recent_rentable),most_recent_rentable:=0]
  buy_decision_data[,DEMAND:=demand1+
                      demand2+
                      demand3+
                      demand4+
                      demand5]
  s=1
  
  simdata = copy(buy_decision_data)
  rm(buy_decision_data);gcQuiet();
  
  simoutput_master <- list()
  
  runidd = unique(simdata$runid)[1]
  styles <- unique(simdata$STYLE_NO)
  
  s=1
  simoutput <- list()
  k = 1
  i = styles[1]
  
  
  for(runidd in unique(simdata$runid)){
    print(runidd)
    for (i in styles) {
      
      output = simulateWith_C(
        inputData = simdata[STYLE_NO == i & runid==runidd,
                            list(HUB=RunLevel,
                                 STYLE_NO,
                                 SIZE_CODE,
                                 DATE=WEEK_START_DATE,
                                 ON_HAND=most_recent_rentable,
                                 leadtime1,
                                 leadtime2,
                                 leadtime3,
                                 leadtime4,
                                 leadtime5,
                                 demand=DEMAND,
                                 demand1,
                                 demand2,
                                 demand3,
                                 demand4,
                                 demand5,
                                 ADD_STOCK=PO_RECEIVE,
                                 REMOVE_STOCK=0,
                                 ATTRITION,
                                 ADD_RENTED=0
                            )])
      output[,runid:=runidd]
      
      simoutput[[k]] <- output
      rm(output);gcQuiet();
      
      k = k + 1
    }
    
  }
  
  simoutput_master <- rbindlist(simoutput)
  rm(simoutput);gcQuiet()
  
  
  
  by=1
  for(by in 1:5){
    bycfix_col = paste0("BuyCanFix",by)
    unfulfill_col = paste0("Unfulfilled_RESV",by)
    simrent_col = paste0("SIMRENTS",by)
    bycfix_rolling_col = paste0("BuyCanFix_rolling",by)
    ltcol = paste0("leadtime",by)
    
    
    simoutput_master[,c(bycfix_col):= ifelse(DATE<(no_order_period_end),
                                             0,
                                             get(unfulfill_col))]
    
    
    simoutput_master[,c(bycfix_rolling_col):=roll_sumr(get(bycfix_col),
                                                       in_use_cycle_groups[[by]]),
                     .(runid,
                       HUB,
                       STYLE_NO,
                       SIZE_CODE)
                     
    ]
    
    if(by==1){
      simoutput_master[,BuyCanFix:=get(bycfix_col)]
      simoutput_master[,BuyCanFix_rolling:=get(bycfix_rolling_col)]
      
    }else{
      simoutput_master[,BuyCanFix:=BuyCanFix + get(bycfix_col)]
      simoutput_master[,BuyCanFix_rolling:=BuyCanFix_rolling + get(bycfix_rolling_col)]
      
    }
    
    simoutput_master[,c(bycfix_col,
                        unfulfill_col,
                        simrent_col,
                        bycfix_rolling_col):=NULL]
    gcQuiet()
  }

# if(F){  
  for(by in 1:5){
    demand_col = paste0("demand",by)
    demand_rolling_col = paste0("demand_rolling",by)
    ltcol = paste0("leadtime",by)
    
    simoutput_master[,c(demand_rolling_col):=roll_sumr(get(demand_col),
                                                       in_use_cycle_groups[[by]]),
                     .(runid,
                       HUB,
                       STYLE_NO,
                       SIZE_CODE)
                     
    ]
    
    if(by==1){
      simoutput_master[,PeakDemand:=get(demand_col)]
      simoutput_master[,PeakDemand_rolling:=get(demand_rolling_col)]
      
    }else{
      simoutput_master[,PeakDemand:=PeakDemand + get(demand_col)]
      simoutput_master[,PeakDemand_rolling:=PeakDemand_rolling + get(demand_rolling_col)]
      
    }
    
    simoutput_master[,c(demand_col,
                        demand_rolling_col):=NULL]
    gcQuiet()
  }
# } 
  rr=1
  for(rr in 1:5){
    ltcol = paste0("leadtime",rr)
    demandcol = paste0("demand",rr)
    simoutput_master[,c(ltcol,demandcol):=NULL]
  }
  
  
  simoutput_master[is.na(BuyCanFix_rolling),BuyCanFix_rolling:=0]
  # if(F){
  simoutput_master[is.na(PeakDemand_rolling),PeakDemand_rolling:=0]
  # }
  setnames(simoutput_master,"DATE","WEEK_START_DATE")
  simoutput_master[,max_peak:=max(BuyCanFix_rolling),
                   list(runid,
                        HUB,
                        STYLE_NO,
                        SIZE_CODE
                   )]
  simoutput_master = simoutput_master[order(runid,
                                            HUB,
                                            STYLE_NO,
                                            SIZE_CODE,
                                            WEEK_START_DATE)]
  
  simoutput_master[,rollingsum:=cumsum(BuyCanFix),list(runid,
                                                       HUB,
                                                       STYLE_NO,
                                                       SIZE_CODE
  )]
  
  simoutput_master[,BUY:=ifelse(rollingsum<=max_peak,BuyCanFix,0)]
  
  simoutput_master[rollingsum>=max_peak,rank:=1:.N,list(runid,
                                                        HUB,
                                                        STYLE_NO,
                                                        SIZE_CODE)]
  
  simoutput_master[rank==1,BUY:=BuyCanFix-(rollingsum-max_peak)]
  
  simoutput_master[,c("rank","rollingsum","max_peak"):=NULL]
  
  simoutput_master[,c("RECEIVABLES","STARTSTOCK"):=NULL]
  
  setnames(simoutput_master,"ON_HAND","RENTABLE")
  setnames(simoutput_master,"demand","RENTAL_DEMAND")
  setnames(simoutput_master,"SIMRENTS","RENTAL_FULFILLED")
  setnames(simoutput_master,"ADD_STOCK","PO_RECEIVED")
  simoutput_master[,COMPANY:=ifelse(HUB==1799,"MSP","TMW")]
  simoutput_master[,savelevel:=STYLE_NO]
  
  
  
  simoutput_master[,write_to_blob(.SD,
                                  savename = paste0(savelevel[1],
                                                    ".parquet"),
                                  containerconnection = container_datastore,
                                  blobfilepath = file.path("algorithm",
                                                           "order_range",
                                                           global_demand_name
                                  )
  ),list(savelevel)]
  
  
  rm(simdata,
     simoutput_master);gc();
  
  
}




print("buy_decision_order_range_create.R is completed")








