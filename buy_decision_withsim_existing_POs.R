rm(list=ls())
library(data.table)
library(RcppRoll)
library(configr)
library(AzureStor)
library(inventRlibrary)


if (Sys.info()["user"] == "onur.uzun") {
  setwd("C:/users/onur.uzun/tailoredbrands")
  source("initialize.R")
}

if (Sys.info()["user"] == "koray") {
  setwd("C:\\git\\tailoredbrands")
  source("initialize.R")
}

if (Sys.info()["user"] == "yagiz.yaman") {
  setwd("C:/Users/yagiz.yaman/tailoredbrands")
  source("initialize.R")
}

if (Sys.info()["user"] == "simay.colak") {
  setwd("C:\\git\\tailoredbrands")
  source("initialize.R")
}

### CORE PARAMETERS - START
earliest_arrive_date = '2023-02-05'
global_demand_name = "Scenario=inuse_distbyHUB_orderengine"

order_range_count = 50
### CORE PARAMETERS - END


data_access("future_inventory")

buy_decision_data_master <- future_inventory[,list(COMPANY
                                                   ,HUB
                                                   ,STYLE_NO
                                                   ,SIZE_CODE 
                                                   ,WEEK_START_DATE
                                                   ,PO_RECEIVE
                                                   ,most_recent_rentable
                                                   ,is_future_date
                                                   )]
rm(future_inventory)

buy_decision_data_master[is.na(buy_decision_data_master)] <- 0


simrundate <- min(buy_decision_data_master[is_future_date == 1]$WEEK_START_DATE)
buy_decision_data_master <- 
  buy_decision_data_master[WEEK_START_DATE >= simrundate]
buy_decision_data_master[WEEK_START_DATE > simrundate, 
                         most_recent_rentable := 0]
buy_decision_data_master[, is_future_date := NULL]

data_access("max_peak_fc_dynamic")

run_list = AzureStor::list_blobs(container_datastore,
                                 dir = file.path("algorithm", 
                                                 "global_demand",
                                                 global_demand_name),
                                 prefix = NULL,
                                 recursive = T)$name

if (FALSE) {
  # for a specific run
  global_demand = read_parquetR(
    pathtoread = file.path("algorithm",
                           "global_demand",
                           global_demand_name),
    containerconnection = container_datastore,
    filelocation = "azure",
    where = "runid == 1 & STYLE_NO %in% c('2130')"
  )
}


global_demand = read_parquetR(
  pathtoread = data_filenames$globaldemand_beforeloop,
  containerconnection = container_datastore,
  filelocation = "azure"
)

global_demand=global_demand[WEEK_START_DATE<='2023-12-31']


join_scope = global_demand[,.N,list(COMPANY,HUB,STYLE_NO,SIZE_CODE)]
join_scope[, N := NULL]
setkey(join_scope, COMPANY, STYLE_NO, SIZE_CODE, HUB)
setkey(buy_decision_data_master, COMPANY, STYLE_NO, SIZE_CODE, HUB)

buy_decision_data = join_scope[buy_decision_data_master,
                               nomatch = 0]
rm(join_scope)

global_demand[, c("forecast_with_error", "forecast") := NULL]
setkey(global_demand,
       COMPANY,
       HUB,
       STYLE_NO,
       SIZE_CODE,
       WEEK_START_DATE)
setkey(buy_decision_data,
       COMPANY,
       HUB,
       STYLE_NO,
       SIZE_CODE,
       WEEK_START_DATE)

simoutput_master <- buy_decision_data[global_demand]
rm(global_demand);gcQuiet()
# buy_decision_data[is.na(PO_RECEIVE),PO_RECEIVE:=0]
simoutput_master <- simoutput_master[WEEK_START_DATE >= simrundate]

simoutput_master[is.na(most_recent_rentable), 
                 most_recent_rentable := 0]

simoutput_master[, periodtag := ifelse(WEEK_START_DATE < first_half_datelimit,
                                       "first_half", "second_half")
]

simoutput_master[, BUY := 0]
simoutput_master[, runid := 1]
simoutput_master[, BuyCanFix_rolling := 0]
setnames(simoutput_master, "PO_RECEIVE", "PO_RECEIVED")
simoutput_master[, TRN_IN := 0]
simoutput_master[, TRN_OUT := 0]
simoutput_master[PO_RECEIVED > 0]
simoutput_master[is.na(PO_RECEIVED), PO_RECEIVED := 0]

setkey(simoutput_master, COMPANY, HUB, STYLE_NO, SIZE_CODE, periodtag)
setkey(max_peak_fc_dynamic, COMPANY, HUB, STYLE_NO, SIZE_CODE, periodtag)
simoutput_master <- max_peak_fc_dynamic[
  , .(COMPANY, HUB, STYLE_NO, SIZE_CODE, periodtag,
      PeakDemand_rolling = round(PeakForecast_rolling))
][simoutput_master]
simoutput_master[, PeakDemand_rolling_TRN := PeakDemand_rolling]
setnames(simoutput_master,"most_recent_rentable","RENTABLE")

buy_scenario_asis <- rebalance_them_all(inputdata = simoutput_master,
                                        inputtype = "existingpo",
                                        trn_safety = 1.15)
buy_scenario_asis[PO_RECEIVED > 0]
buy_scenario_asis[TRN_IN > 0]
#buy_scenario_asis[SIZE_CODE=="19/ 35" & COMPANY=="TMW"
#                  & (TRN_IN!=0 | TRN_OUT!=0) ][order(WEEK_START_DATE)]
# tek kalan, gelecek 11 olan PO nun tranfer outa gitmesi,
# ilk transfer outun bu kadar azalmasi
# fakat burada dc rebalance date degisince sistem ne yapar?
# mesela PO daha erken olsa ne olacakti

#summary = buy_scenario_asis[,list(TRN_IN),list(COMPANY,STYLE_NO,SIZE_CODE)]


buy_scenario_asis[, PeakDemand_rolling := 0]


#####


scenario_save_to_blob(input = buy_scenario_asis, 
                      naming = "_existingpowithRebalance")







print("buy_decision_withsim_existing_POs.R is completed")






